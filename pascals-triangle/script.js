
const factorial = (x) => {
    let result = x;
    if (result === 1 || result === 0) {
        result = 1;
        return result;
    }
    else {
        for (let i = x - 1; i > 0 ; i--) {
            result = result * i;
        }
        return result;
    }
}

const pascal = (a, b) => {
    return factorial(a) / (factorial(b) * factorial(a-b));
}

const buildTriangle = (x) => {
    let triangleHtml = "";
    for (let i=0; i < x+1; i++) {
        let row = [];
        for (let z=0; z < i+1; z++) {
            row.push(`<h1 class="item">${pascal(i, z)}</h1>`);
        }
        let rowHtml = `<div class="row">${row.join("")}</div>`;
        triangleHtml = triangleHtml + rowHtml;
    }
    return triangleHtml;
}

document.getElementById("button").addEventListener("click", () => {
    let rows = parseInt(document.getElementById("rows").value);
    document.getElementById("triangle").innerHTML = buildTriangle(rows);
});

